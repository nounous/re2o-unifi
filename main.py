from configparser import ConfigParser
import socket

import subprocess
import importlib
import requests
import sys

from pymongo import MongoClient
from re2oapi import Re2oAPIClient

config = ConfigParser()
config.read('config.ini')

api_hostname = config.get('Re2o', 'hostname')
api_password = config.get('Re2o', 'password')
api_username = config.get('Re2o', 'username')


def connect_mongo():
    ## Connexion mongodb
    client = MongoClient("mongodb://localhost:27117")
    db = client.ace
    device = db['device']
    return device 

def regen_unifi(api_client):
    """Replace name in unifi controler"""
    connection = connect_mongo()

    for device in api_client.list_hostmacip():
        connection.find_one_and_update({'ip': str(device['ipv4'])}, {'$set': {'name': str(device['hostname'])}})

api_client = Re2oAPIClient(api_hostname, api_username, api_password)
client_hostname = socket.gethostname().split('.', 1)[0]

for arg in sys.argv:
    if arg == "--force":
        regen_unifi(api_client)

for service in api_client.list_servicesregen():
    if service['hostname'] == client_hostname and \
            service['service_name'] == 'unifi-ap-names' and \
            service['need_regen']:
        regen_unifi(api_client)
        api_client.patch(service['api_url'], data={'need_regen': False})
